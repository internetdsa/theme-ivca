<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment', 'embed'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    collect(['get_header', 'wp_head'])->each(function ($tag) {
        ob_start();
        do_action($tag);
        $output = ob_get_clean();
        remove_all_actions($tag);
        add_action($tag, function () use ($output) {
            echo $output;
        });
    });
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

add_filter('sage/template/home/data', function (array $data) {
    // HERO
    $data['hero_imagem_thumbnail']                       = get_the_post_thumbnail_url(get_the_ID(), 'background-hero');
    $data['hero_url_video_assistir_video']               = get_field('home_hero_url_assistir_video');
    if(!empty($data['hero_url_video_assistir_video']) && strpos($data['hero_url_video_assistir_video'], 'watch') !== false) {
        $data['hero_url_video_assistir_video'] = explode('v=', $data['hero_url_video_assistir_video']);
        $data['hero_url_video_assistir_video'] = 'https://www.youtube.com/embed/' . explode('&', $data['hero_url_video_assistir_video'][1])[0];
    }
    $data['hero_url_video_background']                   = get_field('home_hero_url_background');
    $data['hero_titulo']                                 = get_field('home_hero_titulo');
    $data['hero_resumo']                                 = get_field('home_hero_resumo');
    $data['hero_link_saiba_mais']                        = get_field('home_hero_url_pagina_saiba_mais');

    $data['hero_areas_list']                             = array_map(function($item){
        return [
            'thumbnail' => get_the_post_thumbnail_url($item->ID, 'thumb-areas-slide'),
            'title'     => $item->post_title,
            'link'      => get_field('home_hero_url_pagina_slide_de_area') ?? get_permalink($item->ID),
        ]; 
    }, get_posts([
        'post_type'        => 'areas',
        'post_status'      => 'publish',
        'posts_per_page'   => -1,
        'suppress_filters' => false,
        'orderby'          => 'title',
        'order'            => 'ASC',
    ]));

    // O IVCA
    $data['o_ivca_titulo']                               = get_field('home_o_ivca_titulo');
    $data['o_ivca_resumo']                               = get_field('home_o_ivca_resumo');
    $data['o_ivca_link_saiba_mais']                      = get_field('home_o_ivca_url_pagina_saiba_mais');

    // PESQUISADORES
    $data['pesquisadores_titulo']                        = get_field('home_pesquisadores_titulo');
    $data['pesquisadores_resumo']                        = get_field('home_pesquisadores_resumo');
    $data['pesquisadores_link_pagina_saiba_mais']        = get_field('home_pesquisadores_url_pagina_saiba_mais');
    $data['pesquisadores_url_imagem_lateral']            = get_field('home_pesquisadores_url_imagem_lateral');

    // AGENDE UMA VISITA
    $data['agende_visita_formulario']                    = get_field('home_agende_visita_formulario');
    $data['agende_visita_mostrar_escritorios_regionais'] = get_field('home_agende_visita_mostrar_escritorios_regionais');

    if ($data['agende_visita_mostrar_escritorios_regionais']) {
        // Sede e escritórios
    $data['agende_visita_escritorios_list']              = array_filter(array_map(function($item) use (&$data){
            $objeto = [
                'title'    => $item->post_title,
                'telefone' => get_field('telefone', $item->ID),
            ];

            if(!get_field('sede', $item->ID)){
                return $objeto; 
            }

            $data['escritorios_sede'] = $objeto;
        }, get_posts([
            'post_type'        => 'escritorios',
            'post_status'      => 'publish',
            'posts_per_page'   => -1,
            'suppress_filters' => false,
            'orderby'          => 'title',
            'order'            => 'ASC',
        ])));
    }

    // VAMOS CONVERSAR
    $data['vamos_conversar_formulario']                  = get_field('home_vamos_conversar_formulario');

    return $data;
});

add_filter('sage/template/template-institucional/data', function (array $data) {
    // Hero
    $data['hero_titulo']                      = get_the_title();
    $data['hero_resumo']                      = get_the_excerpt();
    $data['hero_url_background']              = get_the_post_thumbnail_url(get_the_ID(), 'background-hero');

    // O melhor da vida
    $data['o_melhor_da_vida_titulo']          = get_field('o_melhor_da_vida_titulo');
    $data['o_melhor_da_vida_resumo']          = get_field('o_melhor_da_vida_resumo');
    $data['o_melhor_da_vida__url_imagem_top'] = get_field('o_melhor_da_vida__url_imagem_top');
    $data['o_melhor_da_vida_img_video']       = get_field('o_melhor_da_vida_img_video');
    $data['o_melhor_da_vida_url_video']       = get_field('o_melhor_da_vida_url_video');
    if(!empty($data['o_melhor_da_vida_url_video']) && strpos($data['o_melhor_da_vida_url_video'], 'watch') !== false) {
        $data['o_melhor_da_vida_url_video'] = explode('v=', $data['o_melhor_da_vida_url_video']);
        $data['o_melhor_da_vida_url_video'] = 'https://www.youtube.com/embed/' . explode('&', $data['o_melhor_da_vida_url_video'][1])[0];
    }

    // O que faz
    $data['o_que_faz_titulo']                 = get_field('o_que_faz_titulo');
    $data['o_que_faz_resumo']                 = get_field('o_que_faz_resumo');
    $data['o_que_faz_url_imagem_lateral_1']   = get_field('o_que_faz_url_imagem_lateral_1');
    $data['o_que_faz_url_imagem_lateral_2']   = get_field('o_que_faz_url_imagem_lateral_2');

    // Áreas
    $data['areas_titulo']                     = get_field('areas_titulo');
    $data['areas_list']                       = array_map(function($item){
        return [
            'thumbnail'  => get_the_post_thumbnail_url($item->ID, 'thumb-areas-card'),
            'title'      => $item->post_title,
            'nivel'      => get_field('nivel', $item->ID),
            'categorias' => get_field('categorias', $item->ID),
        ]; 
    }, get_posts([
        'post_type'        => 'areas',
        'post_status'      => 'publish',
        'posts_per_page'   => -1,
        'suppress_filters' => false,
        'orderby'          => 'title',
        'order'            => 'ASC',
    ]));

    // Sede e escritórios
    $data['sede_titulo']                      = get_field('sede_titulo');
    $data['escritorios_list']                 = array_filter(array_map(function($item) use (&$data){
        $objeto = [
            'title'    => $item->post_title,
            'endereco' => get_field('endereco', $item->ID),
            'diretor'  => get_field('diretor', $item->ID),
            'e-mail'   => get_field('e-mail', $item->ID),
            'telefone' => get_field('telefone', $item->ID),
        ];

        if(!get_field('sede', $item->ID)){
            return $objeto; 
        }

        $data['escritorios_sede'] = $objeto;
    }, get_posts([
        'post_type'        => 'escritorios',
        'post_status'      => 'publish',
        'posts_per_page'   => -1,
        'suppress_filters' => false,
        'orderby'          => 'title',
        'order'            => 'ASC',
    ])));

    // Agende uma visita
    $data['agende_visita_titulo']             = get_field('agende_titulo');
    $data['agende_visita_formulario']         = get_field('agende_formulario');

    return $data;
});

add_filter('sage/template/template-rede-instituicoes/data', function (array $data) {
    // Hero
    $data['hero_titulo']                       = get_the_title();
    $data['hero_resumo']                       = get_the_excerpt();
    $data['hero_url_background']               = get_the_post_thumbnail_url(get_the_ID(), 'background-hero');

    // O que faz
    $data['sobre_parcerias_resumo_inicio']     = get_field('sobre_parcerias_resumo_inicio');
    $data['sobre_parcerias_resumo_fundo_azul'] = get_field('sobre_parcerias_resumo_fundo_azul');
    $data['sobre_parcerias_resumo_final']      = get_field('sobre_parcerias_resumo_final');
    $data['sobre_parcerias_imagem_ciclo']      = get_field('sobre_parcerias_imagem_ciclo');

    // Instituições parceiras
    $data['instituicoes_titulo']               = get_field('instituicoes_titulo');
    // dd(get_field('instituicoes_categorias'));

    $data['instituicoes_categorias']           = array_map(function($item){
        return [
            'nome_categoria' => $item['categoria']->name,
            'imagem_info'    => $item['imagem_info'],
            'slides'         => array_map(function($instiuicao){ 
                return [
                    'nome_instituicao'   => $instiuicao->post_title,
                    'imagem_instituicao' => get_the_post_thumbnail_url($instiuicao->ID, 'thumb-parceiras-slide'),
                ];
            }, get_posts([
                'post_type'        => 'parceiras',
                'post_status'      => 'publish',
                'posts_per_page'   => -1,
                'suppress_filters' => false,
                'orderby'          => 'title',
                'order'            => 'ASC',
                'tax_query'        => [
                    [
                        'taxonomy' => 'parceiras_categorias',
                        'field'    => 'term_id',
                        'terms'    => $item['categoria']->term_id,
                    ],
                ],
            ])),
        ]; 
    }, get_field('instituicoes_categorias'));

    // Agende uma visita
    $data['agende_visita_titulo']              = get_field('agende_titulo');
    $data['agende_visita_formulario']          = get_field('agende_formulario');

    return $data;
});

add_filter('sage/template/template-conteudos/data', function (array $data) {
    // Hero
    $data['hero_titulo']                   = get_the_title();
    $data['hero_resumo']                   = get_the_excerpt();
    $data['hero_url_background']           = get_the_post_thumbnail_url(get_the_ID(), 'background-hero');

    // Texto com imagem lateral
    $data['bloco_de_texto_titulo']         = get_field('bloco_de_texto_titulo');
    $data['bloco_de_texto_texto']          = get_field('bloco_de_texto_texto');
    $data['bloco_de_texto_imagem_lateral'] = get_field('bloco_de_texto_imagem_lateral');

    // Áreas
    $data['areas_titulo']                  = get_field('areas_titulo');
    $data['areas_list']                    = array_map(function($item){
        return [
            'thumbnail'      => get_the_post_thumbnail_url($item['area']->ID, 'thumb-areas-card'),
            'title'          => $item['area']->post_title,
            'resumo'         => $item['resumo'],
            'url_saiba_mais' => $item['url_saiba_mais'],
        ]; 
    }, get_field('areas_repeater'));

    // Agende uma visita
    $data['agende_visita_titulo']          = get_field('agende_titulo');
    $data['agende_visita_formulario']      = get_field('agende_formulario');

    return $data;
});

add_filter('sage/template/template-dicas-de-saude/data', function (array $data) {
    // Hero
    $data['hero_titulo']                  = get_the_title();
    $data['hero_resumo']                  = get_the_excerpt();
    $data['hero_url_background']          = get_the_post_thumbnail_url(get_the_ID(), 'background-hero');

    // Posts
    $pagina = array_key_exists('pagina', $_GET) ? $_GET['pagina'] : '1';

    $current_lang = apply_filters( 'wpml_current_language', NULL );
    if($current_lang == 'pt-br') {
        $url_feed = 'https://querovidaesaude.com/feed?paged=' . $pagina;
    } elseif($current_lang == 'es') {
        $url_feed = 'https://quierovidaysalud.com/feed?paged=' . $pagina;
    }

    $rss = fetch_feed($url_feed);
    if (!is_wp_error($rss)) {
        $data['posts_list_erro']          = false;
        $data['posts_list_pagina_atual']  = $pagina;
        $maxitems = $rss->get_item_quantity( 9 ); 
        $rss_items = $rss->get_items(0, $maxitems);

// dd($rss);

    $data['posts_list']                   = array_map(function($item){
            
            if ($enclosure = $item->get_enclosure()){
                $thumbnail = $enclosure->get_link();
            } else {
                $thumbnail = asset_path('images/dicas-de-saude-sem-imagem.jpg');
            }

            return [
                'thumbnail'      => $thumbnail,
                'title'          => $item->get_title(),
                'resumo'         => substr($item->get_description(), 0, strpos($item->get_description(), "</p>")+4), // Pega apenas o primeiro parágrafo
                'url_saiba_mais' => $item->get_permalink(),
            ]; 
        }, $rss_items);
    }else{
        $data['posts_list_erro']          = true;
    }

    // Agende uma visita
    $data['agende_visita_titulo']         = get_field('agende_titulo');
    $data['agende_visita_formulario']     = get_field('agende_formulario');

    return $data;
});

add_filter('sage/template/template-pesquisadores/data', function (array $data) {
    // Hero
    $data['hero_titulo']               = get_the_title();
    $data['hero_resumo']               = get_the_excerpt();
    $data['hero_url_background']       = get_the_post_thumbnail_url(get_the_ID(), 'background-hero');

    // Dúvidas
    $data['duvidas_paragrafo_inicial'] = get_field('duvidas_paragrafo_inicial');
    $data['duvidas_titulo']            = get_field('duvidas_titulo');
    $data['duvidas_imagem_lateral']    = get_field('duvidas_imagem_lateral');
    $data['duvidas_resumo']            = get_field('duvidas_resumo');

    // Pesquisadores
    $data['pesquisadores_list']        = array_map(function($item){ 
        return [
            'thumbnail' => get_the_post_thumbnail_url($item->ID, 'foto-pesquisador'),
            'nome'      => $item->post_title,
            'curriculo' => get_field('curriculo', $item->ID),
        ]; 
    }, get_posts([
        'post_type'        => 'pesquisadores',
        'post_status'      => 'publish',
        'posts_per_page'   => -1,
        'suppress_filters' => false,
        'orderby'          => 'title',
        'order'            => 'ASC',
    ]));

    // Agende uma visita
    $data['agende_visita_titulo']      = get_field('agende_titulo');
    $data['agende_visita_formulario']  = get_field('agende_formulario');

    return $data;
});

add_filter('sage/template/template-conteudos-adicionais/data', function (array $data) {
    // Hero
    $data['hero_titulo']                   = get_the_title();
    $data['hero_resumo']                   = get_the_excerpt();
    $data['hero_url_background']           = get_the_post_thumbnail_url(get_the_ID(), 'background-hero');

    // Texto com imagem lateral
    $data['bloco_de_texto_texto']          = get_field('conteudos_especiais_texto');
    $data['bloco_de_texto_url_botao']      = get_field('conteudos_especiais_url_botao_clique_aqui');
    $data['bloco_de_texto_imagem_lateral'] = get_field('conteudos_especiais_imagem_lateral');

    // Agende uma visita
    $data['agende_visita_titulo']          = get_field('agende_titulo');
    $data['agende_visita_formulario']      = get_field('agende_formulario');

    return $data;
});

add_filter('sage/template/template-consultores/data', function (array $data) {
    // Hero
    $data['hero_titulo']                     = get_the_title();
    $data['hero_resumo']                     = get_the_excerpt();
    $data['hero_url_background']             = get_the_post_thumbnail_url(get_the_ID(), 'background-hero');

    $data['afc_imagem_de_fundo']             = get_field('afc_imagem_de_fundo');
    $data['afc_logo_afc']                    = get_field('afc_logo_afc');
    $data['afc_texto']                       = get_field('afc_texto');
    $data['afc_url_botao_acessar_afc']       = get_field('afc_url_botao_acessar_afc');
    $data['afc_url_botao_quero_cursar']      = get_field('afc_url_botao_quero_cursar');

    $data['consultor_imagem_de_fundo']       = get_field('consultor_imagem_de_fundo');
    $data['consultor_texto']                 = get_field('consultor_texto');
    $data['consultor_url_botao_acessar']     = get_field('consultor_url_botao_acessar');

    // Texto com imagem lateral
    $data['o_melhor_da_vida_titulo']         = get_field('o_melhor_da_vida_titulo');
    $data['o_melhor_da_vida_texto']          = get_field('o_melhor_da_vida_texto');
    $data['o_melhor_da_vida_imagem_lateral'] = get_field('o_melhor_da_vida_imagem_lateral');

    // Agende uma visita
    $data['agende_visita_titulo']            = get_field('quero_mais_informacoes_titulo');
    $data['agende_visita_subtitulo']         = get_field('quero_mais_informacoes_subtitulo');
    $data['agende_visita_formulario']        = get_field('quero_mais_informacoes_formulario');

    return $data;
});

add_filter('sage/template/template-empresas-certificadas/data', function (array $data) {
    // Hero
    $data['hero_titulo']                     = get_the_title();
    $data['hero_resumo']                     = get_the_excerpt();
    $data['hero_url_background']             = get_the_post_thumbnail_url(get_the_ID(), 'background-hero');

    // Níveis
    $data['niveis_list']                     = array_map(function($nivel){ 
        // dd($nivel);
        return [
            'thumbnail' => get_field('imagem_de_fundo', $nivel),
            'cor'       => get_field('cor_de_fundo', $nivel),
            'nome'      => strtoupper($nivel->name),
            'resumo'    => $nivel->description,
            'empresas'  => array_map(function($empresa){ 
                    return [
                        'thumbnail' => get_the_post_thumbnail_url($empresa->ID, 'thumb-logo-certificada'),
                        'nome'      => $empresa->post_title,
                    ]; 
                }, get_posts([
                    'post_type'        => 'emp-certificadas',
                    'post_status'      => 'publish',
                    'posts_per_page'   => -1,
                    'suppress_filters' => false,
                    'orderby'          => 'title',
                    'order'            => 'ASC',
                    'tax_query'        => [
                        [
                            'taxonomy' => 'niveis',
                            'field'    => 'term_id',
                            'terms'    => $nivel->term_id,
                        ],
                    ],
                ])),
        ]; 
    }, get_terms([
        'taxonomy'      => 'niveis',
        'hide_empty'    => false,
    ]));

    // Agende uma visita
    $data['agende_visita_titulo']            = get_field('agende_titulo');
    $data['agende_visita_formulario']        = get_field('agende_formulario');

    return $data;
});

add_filter('sage/template/template-certificacao/data', function (array $data) {
    // Hero
    $data['hero_titulo']                  = get_the_title();
    $data['hero_resumo']                  = get_the_excerpt();
    $data['hero_url_background']          = get_the_post_thumbnail_url(get_the_ID(), 'background-hero');

    // Empresas
    $data['empresas_certificadas_texto']  = get_field('empresas_certificadas_texto');
    $data['empresas_certificadas_titulo'] = get_field('empresas_certificadas_titulo');
    $data['empresas_list']                = array_map(function($empresa){ 
            return [
                'thumbnail' => get_the_post_thumbnail_url($empresa->ID, 'thumb-logo-certificada'),
                'nome'      => $empresa->post_title,
            ]; 
        }, get_posts([
            'post_type'        => 'emp-certificadas',
            'post_status'      => 'publish',
            'posts_per_page'   => -1,
            'suppress_filters' => false,
            'orderby'          => 'title',
            'order'            => 'ASC',
        ]));

    // Como identificar
    $data['como_identificar_titulo']      = get_field('como_identificar_titulo');
    $data['como_identificar_texto']       = get_field('como_identificar_texto');

    // FAQ
    $data['faq_list']                     = array_map(function($item){
        return $item + [
            'hash'       => md5(uniqid("")),
        ]; 
    }, get_field('faq_perguntas'));

    // Agende uma visita
    $data['agende_visita_titulo']         = get_field('agende_titulo');
    $data['agende_visita_formulario']     = get_field('agende_formulario');

    return $data;
});

/**
 * Render comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );

    $data = collect(get_body_class())->reduce(function ($data, $class) use ($comments_template) {
        return apply_filters("sage/template/{$class}/data", $data, $comments_template);
    }, []);

    $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

    if ($theme_template) {
        echo template($theme_template, $data);
        return get_stylesheet_directory().'/index.php';
    }

    return $comments_template;
}, 100);

