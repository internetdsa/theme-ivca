<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), false, null);
    wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), ['jquery'], null, true);

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}, 100);

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil-clean-up');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Menu principal', 'sage')
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));

    /**
     * Add thumb custom size
     * @link https://developer.wordpress.org/reference/functions/add_image_size/
     */
    add_image_size( 'background-hero', 1440, 620, true );
    add_image_size( 'foto-pesquisador', 160, 220, true );
    add_image_size( 'thumb-areas-card', 330, 190, true );
    add_image_size( 'thumb-areas-slide', 190, 160, true );
    add_image_size( 'thumb-dica-saude', 210, 330, true );
    add_image_size( 'thumb-logo-certificada', 173, 58, false );
    add_image_size( 'thumb-parceiras-slide', 930, 556, true );
}, 20);

// Adding excerpt for page
add_post_type_support( 'page', 'excerpt' );

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });

    /**
     * Load translation
     */
    load_theme_textdomain( 'instituto-viva', get_template_directory() . '/language' );
});

if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page(array(
        'page_title'    => __( 'Configuração das Redes Sociais', 'instituto-viva' ),
        'menu_title'    => __( 'Redes Sociais', 'instituto-viva' ),
        'menu_slug'     => 'redes-sociais',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));
    
}

/**
 * Setup Custom Post Types
 */
add_action( 'init', function () {

    // Escritórios
    $labels = [
        'name'                => _x( 'Escritórios', 'Post Type General Name', 'instituto-viva' ),
        'singular_name'       => _x( 'Escritório', 'Post Type Singular Name', 'instituto-viva' ),
        'menu_name'           => __( 'Escritórios', 'instituto-viva' ),
        'parent_item_colon'   => __( 'Pai', 'instituto-viva' ),
        'all_items'           => __( 'Todos', 'instituto-viva' ),
        'view_item'           => __( 'Ver', 'instituto-viva' ),
        'add_new_item'        => __( 'Adicionar Novo', 'instituto-viva' ),
        'add_new'             => __( 'Adicionar Novo', 'instituto-viva' ),
        'edit_item'           => __( 'Editar', 'instituto-viva' ),
        'update_item'         => __( 'Atualizar', 'instituto-viva' ),
        'search_items'        => __( 'Buscar', 'instituto-viva' ),
        'not_found'           => __( 'Não encontrado', 'instituto-viva' ),
        'not_found_in_trash'  => __( 'Não encontrado na lixeira', 'instituto-viva' ),
    ];

    $args = [
        'label'               => __( 'escritorios', 'instituto-viva' ),
        'description'         => __( 'Sede e escritórios regionais', 'instituto-viva' ),
        'labels'              => $labels,
        'supports'            => [ 'title', 'revisions', ],
        'taxonomies'          => [ '' ],
        'hierarchical'        => false,
        'public'              => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => false,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => false,
        'menu_icon'           => 'dashicons-location',
        'capability_type'     => 'page',
    ];

    register_post_type( 'escritorios', $args );

    // Pesquisadores
    $labels = [
        'name'                => _x( 'Pesquisadores', 'Post Type General Name', 'instituto-viva' ),
        'singular_name'       => _x( 'Pesquisador', 'Post Type Singular Name', 'instituto-viva' ),
        'menu_name'           => __( 'Pesquisadores', 'instituto-viva' ),
        'parent_item_colon'   => __( 'Pai', 'instituto-viva' ),
        'all_items'           => __( 'Todos', 'instituto-viva' ),
        'view_item'           => __( 'Ver', 'instituto-viva' ),
        'add_new_item'        => __( 'Adicionar Novo', 'instituto-viva' ),
        'add_new'             => __( 'Adicionar Novo', 'instituto-viva' ),
        'edit_item'           => __( 'Editar', 'instituto-viva' ),
        'update_item'         => __( 'Atualizar', 'instituto-viva' ),
        'search_items'        => __( 'Buscar', 'instituto-viva' ),
        'not_found'           => __( 'Não encontrado', 'instituto-viva' ),
        'not_found_in_trash'  => __( 'Não encontrado na lixeira', 'instituto-viva' ),
    ];

    $args = [
        'label'               => __( 'pesquisadores', 'instituto-viva' ),
        'description'         => __( 'Pesquisadores', 'instituto-viva' ),
        'labels'              => $labels,
        'supports'            => [ 'title', 'revisions', 'thumbnail', ],
        'taxonomies'          => [ '' ],
        'hierarchical'        => false,
        'public'              => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => false,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => false,
        'menu_icon'           => 'dashicons-search',
        'capability_type'     => 'page',
    ];

    register_post_type( 'pesquisadores', $args );

    // Pesquisadores
    $labels = [
        'name'                => _x( 'Áreas', 'Post Type General Name', 'instituto-viva' ),
        'singular_name'       => _x( 'Área', 'Post Type Singular Name', 'instituto-viva' ),
        'menu_name'           => __( 'Áreas', 'instituto-viva' ),
        'parent_item_colon'   => __( 'Pai', 'instituto-viva' ),
        'all_items'           => __( 'Todas', 'instituto-viva' ),
        'view_item'           => __( 'Ver', 'instituto-viva' ),
        'add_new_item'        => __( 'Adicionar Nova', 'instituto-viva' ),
        'add_new'             => __( 'Adicionar Nova', 'instituto-viva' ),
        'edit_item'           => __( 'Editar', 'instituto-viva' ),
        'update_item'         => __( 'Atualizar', 'instituto-viva' ),
        'search_items'        => __( 'Buscar', 'instituto-viva' ),
        'not_found'           => __( 'Não encontrada', 'instituto-viva' ),
        'not_found_in_trash'  => __( 'Não encontrada na lixeira', 'instituto-viva' ),
    ];

    $args = [
        'label'               => __( 'areas', 'instituto-viva' ),
        'description'         => __( 'Áreas', 'instituto-viva' ),
        'labels'              => $labels,
        'supports'            => [ 'title', 'revisions', 'thumbnail', ],
        'taxonomies'          => [ '' ],
        'hierarchical'        => false,
        'public'              => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => false,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => false,
        'menu_icon'           => 'dashicons-image-filter',
        'capability_type'     => 'page',
    ];

    register_post_type( 'areas', $args );

    // Empresas certificadas
    $labels = [
        'name'                => _x( 'Empresas Certificadas', 'Post Type General Name', 'instituto-viva' ),
        'singular_name'       => _x( 'Empresa Certificada', 'Post Type Singular Name', 'instituto-viva' ),
        'menu_name'           => __( 'Empresas Certificadas', 'instituto-viva' ),
        'parent_item_colon'   => __( 'Pai', 'instituto-viva' ),
        'all_items'           => __( 'Todas', 'instituto-viva' ),
        'view_item'           => __( 'Ver', 'instituto-viva' ),
        'add_new_item'        => __( 'Adicionar Nova', 'instituto-viva' ),
        'add_new'             => __( 'Adicionar Nova', 'instituto-viva' ),
        'edit_item'           => __( 'Editar', 'instituto-viva' ),
        'update_item'         => __( 'Atualizar', 'instituto-viva' ),
        'search_items'        => __( 'Buscar', 'instituto-viva' ),
        'not_found'           => __( 'Não encontrada', 'instituto-viva' ),
        'not_found_in_trash'  => __( 'Não encontrada na lixeira', 'instituto-viva' ),
    ];

    $args = [
        'label'               => __( 'emp-certificadas', 'instituto-viva' ),
        'description'         => __( 'Empresas Certificadas', 'instituto-viva' ),
        'labels'              => $labels,
        'supports'            => [ 'title', 'revisions', 'thumbnail', ],
        'taxonomies'          => [ 'niveis' ],
        'hierarchical'        => false,
        'public'              => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => false,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => false,
        'menu_icon'           => 'dashicons-awards',
        'capability_type'     => 'page',
    ];

    register_post_type( 'emp-certificadas', $args );

    // Níveis
    $labels = array(
        'name' => _x( 'Níveis', 'taxonomy general name', 'instituto-viva' ),
        'singular_name' => _x( 'Nível', 'taxonomy singular name', 'instituto-viva' ),
        'search_items' =>  __( 'Buscar', 'instituto-viva' ),
        'all_items' => __( 'Todos', 'instituto-viva' ),
        'parent_item' => __( 'Pai', 'instituto-viva' ),
        'parent_item_colon' => __( 'Pai:', 'instituto-viva' ),
        'edit_item' => __( 'Editar', 'instituto-viva' ), 
        'update_item' => __( 'Atualizar', 'instituto-viva' ),
        'add_new_item' => __( 'Adicionar Nova', 'instituto-viva' ),
        'new_item_name' => __( 'Adicionar Nova', 'instituto-viva' ),
        'menu_name' => __( 'Níveis', 'instituto-viva' ),
    );    

    register_taxonomy( 'niveis', 
        ['emp-certificadas'], 
        [
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
        ]
    );

    // Instituições Parceiras
    $labels = [
        'name'                => _x( 'Instituições Parceiras', 'Post Type General Name', 'instituto-viva' ),
        'singular_name'       => _x( 'Instituição Parceira', 'Post Type Singular Name', 'instituto-viva' ),
        'menu_name'           => __( 'Instituições Parceiras', 'instituto-viva' ),
        'parent_item_colon'   => __( 'Pai', 'instituto-viva' ),
        'all_items'           => __( 'Todas', 'instituto-viva' ),
        'view_item'           => __( 'Ver', 'instituto-viva' ),
        'add_new_item'        => __( 'Adicionar Nova', 'instituto-viva' ),
        'add_new'             => __( 'Adicionar Nova', 'instituto-viva' ),
        'edit_item'           => __( 'Editar', 'instituto-viva' ),
        'update_item'         => __( 'Atualizar', 'instituto-viva' ),
        'search_items'        => __( 'Buscar', 'instituto-viva' ),
        'not_found'           => __( 'Não encontrada', 'instituto-viva' ),
        'not_found_in_trash'  => __( 'Não encontrada na lixeira', 'instituto-viva' ),
    ];

    $args = [
        'label'               => __( 'parceiras', 'instituto-viva' ),
        'description'         => __( 'Instituições Parceiras', 'instituto-viva' ),
        'labels'              => $labels,
        'supports'            => [ 'title', 'revisions', 'thumbnail', ],
        'taxonomies'          => [ 'parceiras_categorias' ],
        'hierarchical'        => false,
        'public'              => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => false,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => false,
        'menu_icon'           => 'dashicons-building',
        'capability_type'     => 'page',
    ];

    register_post_type( 'parceiras', $args );

    // Níveis
    $labels = array(
        'name' => _x( 'Categorias', 'taxonomy general name', 'instituto-viva' ),
        'singular_name' => _x( 'Categoria', 'taxonomy singular name', 'instituto-viva' ),
        'search_items' =>  __( 'Buscar', 'instituto-viva' ),
        'all_items' => __( 'Todos', 'instituto-viva' ),
        'parent_item' => __( 'Pai', 'instituto-viva' ),
        'parent_item_colon' => __( 'Pai:', 'instituto-viva' ),
        'edit_item' => __( 'Editar', 'instituto-viva' ), 
        'update_item' => __( 'Atualizar', 'instituto-viva' ),
        'add_new_item' => __( 'Adicionar Nova', 'instituto-viva' ),
        'new_item_name' => __( 'Adicionar Nova', 'instituto-viva' ),
        'menu_name' => __( 'Categorias', 'instituto-viva' ),
    );    

    register_taxonomy( 'parceiras_categorias', 
        ['parceiras'], 
        [
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
        ]
    );
});
