{{--
  Template Name: Dicas de saúde
--}}

@extends('layouts.app')

@section('content')
  @include('partials.hero')
  @include('partials.dicas-de-saude.lista-de-posts')
  @include('partials.contato.agende-visita')
@endsection
