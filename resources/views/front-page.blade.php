@extends('layouts.app')

@section('content')
	@include('partials.front-page.hero')
	@include('partials.front-page.o-ivca')
	@include('partials.front-page.agende-visita')
	@include('partials.front-page.vamos-conversar')
    @include('partials.front-page.modal-maissaude')
@endsection
