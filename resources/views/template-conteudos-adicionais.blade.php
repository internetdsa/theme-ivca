{{--
  Template Name: Conteúdos adicionais
--}}

@extends('layouts.app')

@section('content')
  @include('partials.hero')
  @include('partials.conteudos.texto-com-imagem-lateral')
  @include('partials.contato.agende-visita')
@endsection
