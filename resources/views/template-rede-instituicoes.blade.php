{{--
  Template Name: Rede de Instituições
--}}

@extends('layouts.app')

@section('content')
  @include('partials.hero')
  @include('partials.institucional-rede.texto-bloco-imagem')
  @include('partials.institucional-rede.instituicoes-parceiras')
  @include('partials.contato.agende-visita')
@endsection
