{{--
  Template Name: Pesquisadores
--}}

@extends('layouts.app')

@section('content')
  @include('partials.hero')
  @include('partials.pesquisadores.conteudo-imagem')
  @include('partials.pesquisadores.lista-pesquisadores')
  @include('partials.contato.agende-visita')
@endsection
