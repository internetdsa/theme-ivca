<section class="hero">
	<div class="video-wrapper">
		<video preload="auto" poster="{{ $hero_imagem_thumbnail }}" autoplay="" muted="" loop="">
			<source src="{{ $hero_url_video_background }}" type="video/mp4">
		</video>
	</div>
	<div class="container">
		@if($hero_url_video_assistir_video && !empty($hero_url_video_assistir_video))
			<!-- Modal -->
			<div class="modal fade" id="video-modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<!-- 16:9 aspect ratio -->
							<div class="embed-responsive embed-responsive-16by9">
								<iframe class="embed-responsive-item" src="" id="video" allowscriptaccess="always" allow="autoplay"></iframe>
							</div>
						</div>
					</div>
				</div>
			</div>
		@endif
		<div class="row">
			<div class="share-e-idioma">
				<div class="play-share float-left">
					@if($hero_url_video_assistir_video && !empty($hero_url_video_assistir_video))
						<a href="#" class="play video-btn" data-toggle="modal" data-src="{{ $hero_url_video_assistir_video }}" data-target="#video-modal"><img src="@asset('images/icon-play.png')" alt=""> {{ __('Assistir ao vídeo', 'instituto-viva') }}</a>
					@endif
					<a href="#" class="share"><img src="@asset('images/icon-share.png')" alt=""></a>
				</div>
				<div class="switcher-idioma float-right">
				    @if (function_exists('icl_get_languages'))
				    	@php
				        	$languages = icl_get_languages('skip_missing=0');
			        	@endphp
				        @if(count($languages) > 1)
				            @foreach($languages as $l)
								<a href="{{ $l['url'] }}" class="{{ $l['language_code'] }}">{{ strtoupper(substr($l['language_code'], 0, 2)) }}</a>
				            @endforeach
				        @endif
				    @endif
				</div>
			</div>
			<div class="col-lg-12">
				<h1>{{ $hero_titulo }}</h1>
			</div>
			<div class="col-lg-5 col-md-12">
				<div class="resumo">
					<p>{{ $hero_resumo }}.</p>
					<a href="{{ $hero_link_saiba_mais }}">{{ __('SAIBA MAIS', 'instituto-viva') }} <img src="@asset('images/icon-plus-green.png')" alt=""></a>
				</div>
			</div>
			<div class="offset-lg-2 col-lg-5 col-md-12">
				<div class="nossas-areas">
					<span>{{ __('Conteúdo Vida', 'instituto-viva') }}</span>
					<h3>{{ __('CONHEÇA NOSSAS ÁREAS', 'instituto-viva') }}</h3>
					<div class="itens-carousel owl-carousel owl-theme">
						@foreach($hero_areas_list as $area)
							<div class="item" style="background: url('{{ $area['thumbnail'] }}') no-repeat center center;">
								<a href="{{ $area['link'] }}">
									<div class="overlay"><div>{!! $area['title'] !!}</div></div>
								</a>
							</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
</section>