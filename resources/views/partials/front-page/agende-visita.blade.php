<section class="agende-visita bg-full" style="background: rgb(102,204,255) url('@asset('images/temp/fundo_home_agende.png')');">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h2 class="title">{{ __('AGENDE UMA VISITA', 'instituto-viva') }}</h2>
				{!! $agende_visita_formulario !!}
			</div>
			@if($agende_visita_mostrar_escritorios_regionais)
				<div class="col-md-6">
					<h3>{{ __('ESCRITÓRIOS REGIONAIS DO INSTITUTO VIDA', 'instituto-viva') }}</h3>
					<div class="escritorios">
						<div class="escritorio">
							<div class="nome">{{ __('SEDE', 'instituto-viva') }} - {{ $escritorios_sede['title'] }}</div>
							<div class="contato">{{ $escritorios_sede['telefone'] }}</div>
                        </div>
						@foreach($agende_visita_escritorios_list as $escritorio_item)
	                        <div class="escritorio">
								<div class="nome">{{ $escritorio_item['title'] }}</div>
								<div class="contato">{{ $escritorio_item['telefone'] }}</div>
	                        </div>
	                    @endforeach
					</div>
				</div>
			@endif
		</div>
	</div>
</section>
