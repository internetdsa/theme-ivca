<section class="vamos-conversar bg-full" id="vamos-conversar" style="background: rgb(255,255,255) url('@asset('images/temp/fundo_home_agende.png')');">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>
					<div></div>
					<span>{{ __('como podemos te ajudar?', 'instituto-viva') }}</span>
					{{ __('VAMOS CONVERSAR', 'instituto-viva') }}
				</h2>
			</div>
			<div class="col-md-6">
				{!! $vamos_conversar_formulario !!}
			</div>
		</div>
	</div>
</section>
