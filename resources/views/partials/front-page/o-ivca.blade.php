<section class="o-ivca-short">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="o-ivca-short-wrapper">
					<h2>{{ $o_ivca_titulo }}</h2>
					{!! $o_ivca_resumo !!}
					<a href="{{ $o_ivca_link_saiba_mais }}">{{ __('SAIBA MAIS', 'instituto-viva') }} <img src="@asset('images/icon-plus-pink.png')" alt=""></a>
				</div>
			</div>
			<div class="col-md-6">
				<div class="pesquisadores-wrapper">
					<div class="row">
						<div class="col-md-12">
							<h2><span></span> {{ $pesquisadores_titulo }}</h2>
						</div>
						<div class="col-lg-6 col-md-12">
							{!! $pesquisadores_resumo !!}
							<a href="{{ $pesquisadores_link_pagina_saiba_mais }}">{{ __('SAIBA MAIS', 'instituto-viva') }} <img src="@asset('images/icon-plus-blue.png')" alt=""></a>
						</div>
						<div class="col-lg-12 d-none d-sm-none d-md-none d-lg-block">
							<img class="img-direita" src="{{ $pesquisadores_url_imagem_lateral }}" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
