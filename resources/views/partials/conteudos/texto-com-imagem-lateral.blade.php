<section class="texto-com-imagem-lateral">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="container-texto">
					@if(isset($bloco_de_texto_titulo))
						<h2>{{ $bloco_de_texto_titulo }}</h2>
					@endif
					{!! $bloco_de_texto_texto !!}
					@if(isset($bloco_de_texto_url_botao))
						<a href="{{ $bloco_de_texto_url_botao }}" class="btn btn-pink"> {{ __('CLIQUE AQUI', 'instituto-viva') }}</a>
					@endif
				</div>
			</div>
			<div class="col-lg-6 d-none d-xs-none d-sm-none d-md-none d-lg-block d-xl-block">
				<img class="img-direita" src="{{ $bloco_de_texto_imagem_lateral }}" alt="">
			</div>
		</div>
	</div>
</section>
