<section class="template-error">
    <div class="container">
        <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<h1 class="text-center">Erro 404</h1>
            @if (!have_posts())
                <p class="text-center">
                {{ __('Desculpe, mas a página que você está tentando visualizar não existe.', 'instituto-viva') }}
                </p>
                <a href="{{ home_url('/') }}">{{ __('Página inicial', 'instituto-viva') }}</a>
            @endif
			</div>
		</div>
	</div>
</section>