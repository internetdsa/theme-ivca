<section class="areas">
    <div class="container">
        <div class="row">
            @if($areas_titulo)
                <div class="col-md-12">
                    <h4 class="title-eqs-areas text-left">{{ $areas_titulo }}</h4>
                </div>
            @endif
            {{-- Estrutura das cards --}}
            <div class="card-consultant col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @foreach($areas_list as $area_item)
                    @include('partials.componentes.card_area')
                @endforeach
            </div>
        </div>
    </div>
</section>