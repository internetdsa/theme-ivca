<footer class="content-info">
  <div class="container">

    @php dynamic_sidebar('sidebar-footer') @endphp
    <div class="social">
    	<div>
    		{{ __('Instituto Vida', 'instituto-viva') }}
		</div>
    	<div>
            @if(get_field('url_facebook_ivca', 'option'))
    		<a href="{{ get_field('url_facebook_ivca', 'option') }}" target="_blank"><i class="fab fa-facebook-square"></i></a>
            @endif
            @if(get_field('url_instagram_ivca', 'option'))
    		<a href="{{ get_field('url_instagram_ivca', 'option') }}" target="_blank"><i class="fab fa-instagram"></i></a>
            @endif
            @if(get_field('url_twitter_ivca', 'option'))
    		<a href="{{ get_field('url_twitter_ivca', 'option') }}" target="_blank"><i class="fab fa-twitter"></i></a>
            @endif
            @if(get_field('url_youtube_ivca', 'option'))
    		<a href="{{ get_field('url_youtube_ivca', 'option') }}" target="_blank"><i class="fab fa-youtube-square"></i></a>
            @endif
		</div>
    </div>
    <div class="copyright">{{ date("Y") }} Copyright - {{ __('Desenvolvido por', 'instituto-viva') }} SYNERGIC DIGITAL</div>
  </div>
</footer>
