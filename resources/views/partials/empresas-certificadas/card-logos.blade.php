<section class="card-logos">
    <div class="container">
        <div class="row">
            {{-- Primeira Card --}}
            @foreach($niveis_list as $nivel)
                <div class="card-logo-margin col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <div class="card card-logo_setting" style="background-repeat: no-repeat;background: {{ $nivel['cor'] }} url('{{ $nivel['thumbnail'] }}');">
                        <div class="card-body">
                            <h3 class="">{{ __('EMPRESAS CERTIFICADAS', 'instituto-viva') }} {{ $nivel['nome'] }}</h3>
                            <p class="">{!! $nivel['resumo'] !!}</p>
                            <div class="card-display">
                                @foreach($nivel['empresas'] as $empresa)
                                <div class="col-reset col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                    <img class="img-fluid" src="{{ $empresa['thumbnail'] }}" title="{{ $empresa['nome'] }}">
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>