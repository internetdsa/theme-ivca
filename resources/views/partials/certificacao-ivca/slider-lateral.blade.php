<section class="slider-lateral">
    <div class="container">
        <div class="row">
            <div class="p-slider-lateral col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">    
                {!! $empresas_certificadas_texto !!}
            </div>
            
            <div class="slider-margin col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                  
                <div id="carousel-empresas" class="carousel slide slide-empr-cert" data-ride="carousel">
                    <div><h3 class="">{!! $empresas_certificadas_titulo !!}</h3></div>
                    <div class="carousel-inner reset-margin-right">
                        @foreach($empresas_list as $key_empresa => $empresa)
                            <div class="carousel-item{{ $key_empresa == 0 ? ' active' : '' }}">
                                <img class="img-fluid" src="{{ $empresa['thumbnail'] }}" alt="{{ $empresa['nome'] }}">
                            </div>
                        @endforeach
                    </div>
                    <div class="slider-container-seta">
                        <a class="carousel-control-prev seta-slider-left" href="#carousel-empresas" role="button" data-slide="prev">
                            <img class="img-fluid" src="@asset('images/temp/seta-esquerda.png')" alt="">
                        </a>
                        <a class="carousel-control-next seta-slider-right" href="#carousel-empresas" role="button" data-slide="next">
                            <img class="img-fluid" src="@asset('images/temp/seta-direita.png')" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="p-slider-lateral col-xs-12 col-sm-12 col-md-12 col-lg-8 col-xl-6">
                <div class="subtitle-margin-slide">
                        <h4>{!! $como_identificar_titulo !!}</h4>
                </div>
            </div>
            <div class="text-slider-down col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="">
                    {!! $como_identificar_texto !!}
                </div>
            </div>
        </div>
    </div>
</section>