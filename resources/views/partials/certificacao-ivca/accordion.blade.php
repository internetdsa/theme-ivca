<section class="accordion">
    <div class="container">
        <div class="row">
            <div class="accordion-setting col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                @foreach($faq_list as $pergunta)
                    <div class="accordion" id="accordion-{{ $pergunta['hash'] }}">
                        <div class="card reset-card">
                            <div class="card-header reset-card-header" id="pergunta-{{ $pergunta['hash'] }}">
                                <h5 class="mb-0">
                                    <button class="btn btn-link title-accordion" type="button" data-toggle="collapse" data-target="#resposta-{{ $pergunta['hash'] }}" aria-expanded="false" aria-controls="resposta-{{ $pergunta['hash'] }}">
                                    {!! $pergunta['pergunta'] !!}
                                    </button>
                                </h5>
                            </div>
                            <div id="resposta-{{ $pergunta['hash'] }}" class="collapse" aria-labelledby="pergunta-{{ $pergunta['hash'] }}" data-parent="#accordion-{{ $pergunta['hash'] }}">
                                <div class="card-body reset-card-body">
                                    {!! $pergunta['resposta'] !!}
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>