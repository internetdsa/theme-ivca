<section class="text-com-img-esquerda">
    <div class="container">
        <div class="row">
            <div class="tcie-padding col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">    
                <div>
                    <h2>{{ $o_melhor_da_vida_titulo }}</h2>
                </div>
                <div>
                    {!! $o_melhor_da_vida_texto !!}
                </div>
            </div>
            <div class="ajuste-bloco-img col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">    
                <img class="img-fluid" src="{{ $o_melhor_da_vida_imagem_lateral }}">
            </div>
        </div>
    </div>
</section>
