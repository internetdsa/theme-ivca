<section class="card-com-botao">
    <div class="container">
        <div class="row" style="margin-top: -1rem;">
            <div class="reset-padl col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">    
                <div class="img-ccb bg-full" style="background: linear-gradient(rgba(102, 204, 255, 0.8), rgba(102, 204, 255, 0.8)), url('{{ $afc_imagem_de_fundo }}');">
                    <div class="ccb-justify-center">
                        <img class="img-fluid card-padd-img" src="{{ $afc_logo_afc }}">
                    </div>
                    <div class="ccb-par-position">
                        <p class="text-center">{{ $afc_texto }}</p>
                    </div>
                    <div class="ccb-block-btn-left">
                        <a href="{{ $afc_url_botao_quero_cursar }}" class="btn btn-white-ccb">{{ __('QUERO CURSAR', 'instituto-viva') }}</a>
                        <a href="{{ $afc_url_botao_acessar_afc }}" class="btn btn-white-ccb">{{ __('ACESSAR AFC', 'instituto-viva') }}</a>
                    </div>
                </div>
            </div>
            <div class="reset-padr col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">    
                <div class="img-ccb bg-full" style="background: linear-gradient(rgba(153 ,204 ,51, 0.8), rgba(153 ,204 ,51, 0.8)), url('{{ $consultor_imagem_de_fundo }}');">
                    <div class="ccb-pp-download">
                        <p class="text-center ccb-par-direita">{{ $consultor_texto }}</p>
                    </div>
                    <div class="ccb-block-btn-right">
                        <a href="{{ $consultor_url_botao_acessar }}" class="btn btn-marine-ccb">{{ __('ACESSAR', 'instituto-viva') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
