<section class="hero bg-overlay" style="background: url('{{ $hero_url_background }}') no-repeat center center;">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="switcher-idioma float-right">
				    @if (function_exists('icl_get_languages'))
				    	@php
				        	$languages = icl_get_languages('skip_missing=0');
			        	@endphp
				        @if(count($languages) > 1)
				            @foreach($languages as $l)
								<a href="{{ $l['url'] }}" class="{{ $l['language_code'] }}">{{ strtoupper(substr($l['language_code'], 0, 2)) }}</a>
				            @endforeach
				        @endif
				    @endif
				</div>
				<h1>{{ $hero_titulo }}</h1>
			</div>
			<div class="col-lg-5">
				<div class="resumo">
					{!! $hero_resumo !!}
				</div>
			</div>
		</div>
	</div>
</section>
