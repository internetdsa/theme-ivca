<section class="agente-uma-visita bg-full" style="background: rgb(102,204,255) url('@asset('images/temp/fundo_home_agende.png')');">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
			<h2 class="title-agende">{{ $agende_visita_titulo }}</h2>
			<!-- <p>PARA ME TORNAR UM
					CONSULTOR DO INSTITUTO VIDA</p> -->
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
				<div class="form-auv">
					{!! $agende_visita_formulario !!}
				</div>
			</div>
		</div>
	</div>
</section>