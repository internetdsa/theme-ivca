<section class="lista-pesquisadores">
    <div class="container">
        <div class="row">
            @foreach($pesquisadores_list as $pesquisador)
            <div class="block-pad-pesq col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">    
                <div class="row">
                    <!-- <div class="block-list-pesq"> -->
                        <div class="text-container col-sm-7 col-md-7 col-lg-7 col-xl-7">
                            <h4>{{ $pesquisador['nome'] }}</h4>
                            {!! $pesquisador['curriculo'] !!}
                        </div>
                        <div class="img-container col-sm-5 col-md-5 col-lg-5 col-xl-5">
                            <img class="img-fluid" src="{{ $pesquisador['thumbnail'] }}" alt="">
                        </div>
                   <!--  </div> -->
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>