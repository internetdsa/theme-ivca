<section class="conteudo-imagem">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                {{-- Texto normal --}}
                <p class="descricao-inicial">{!! $duvidas_paragrafo_inicial !!}</p>
            </div>
            <div class="pesq-order-two col-xs-12 col-sm-12 col-md-12 col-lg-7 col-xl-7">
                <div class="img-oque-e">
                    <img class="img-fluid mx-auto" src="{{ $duvidas_imagem_lateral }}" alt="">
                </div>
            </div>
            <div class="pesq-order-one col-xs-12 col-sm-12 col-md-12 col-lg-5 col-xl-5">
                <p class="descricao-up-img">{!! $duvidas_titulo !!}</p>
            </div>
            <div class="pesq-order-three col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                {{-- Texto normal --}}
                <div class="bloco-lateral-pesq">
                    {!! $duvidas_resumo !!}
                </div>
            </div>
        </div>
    </div>
</section>
