<section class="instituicoes-parceiras">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <h2 class="text-center">{{ $instituicoes_titulo }}</h2>
            </div>
            {{-- Exemplo com slider --}}
            @foreach($instituicoes_categorias as $key_categorias => $categoria)
                <div class="slider-repeat col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="slider-ip-bkg">
                        <div id="carousel-{{$key_categorias}}" class="carousel slide slider-unidades" data-ride="carousel">
                            <div class="carousel-inner">
                                @foreach($categoria['slides'] as $key_slide => $slide)
                                    <div class="carousel-item{{ $key_slide == 0 ? ' active' : '' }}">
                                        <img class="d-block img-fluid" src="{{ $slide['imagem_instituicao'] }}" alt="">
                                        <div class="caption-slider">
                                            <p>{{ $slide['nome_instituicao'] }}</p>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            @if(count($categoria['slides']) > 1)
                                <a class="carousel-control-prev slider-left" href="#carousel-{{$key_categorias}}" role="button" data-slide="prev">
                                    <img class="d-block img-fluid" src="@asset('images/temp/seta-esquerda.png')" alt="">
                                </a>
                                <a class="carousel-control-next slider-right" href="#carousel-{{$key_categorias}}" role="button" data-slide="next">
                                    <img class="d-block img-fluid" src="@asset('images/temp/seta-direita.png')" alt="">
                                </a>
                            @endif
                          </div>
                          <div class="text-while col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-12">
                            <p class="uni-text">{{ $categoria['nome_categoria'] }}</p>
                          </div>
                          <div class="ri-slide-mt col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-12">
                              <img class="d-block img-fluid img-fluid-uni" src="{{ $categoria['imagem_info'] }}" alt="">
                            </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
