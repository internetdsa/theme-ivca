<section class="texto-bloco-imagem">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                {{-- Texto normal --}}
                {!! $sobre_parcerias_resumo_inicio !!}
                {{-- Bloco azul --}}
                <div class="bloco-bkg">
                    {!! $sobre_parcerias_resumo_fundo_azul !!}
                </div>
                {{-- Texto normal --}}
                {!! $sobre_parcerias_resumo_final !!}
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                <div class="text-center img-tbi d-none d-lg-block d-xl-block">
                    <img class="img-fluid mx-auto" src="{{ $sobre_parcerias_imagem_ciclo }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>