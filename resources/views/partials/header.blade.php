<header>
    <nav class="navbar navbar-expand-lg navbar-light">
        <a class="brand" href="{{ home_url('/') }}"><img src="@asset('images/logo.png')"/></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu-principal" aria-controls="menu-principal" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu( array( 'theme_location' => 'primary_navigation', 'depth' => 2, 'container' => 'div', 'container_class' => 'collapse navbar-collapse', 'container_id' => 'menu-principal', 'menu_class' => 'navbar-nav ml-auto', 'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback', 'walker' => new WP_Bootstrap_Navwalker()) ) !!}
        @endif
    </nav>
</header>
