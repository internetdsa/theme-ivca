<section class="o-melhor-da-vida">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
				<h2>{{ $o_melhor_da_vida_titulo }}</h2>
				{!! $o_melhor_da_vida_resumo !!}
			</div>
			
			<div class="order-omdv col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
				{{-- Imagem que fica acima do video --}}
				<img class="img-direita img-fluid d-none d-sm-none d-md-block" src="{{ $o_melhor_da_vida__url_imagem_top }}" alt="">
				{{-- Video --}}
				<a href="#" class="order-omdv video-btn" data-toggle="modal" data-src="{{ $o_melhor_da_vida_url_video }}" data-target="#video-modal">
					<img src="{{ $o_melhor_da_vida_img_video }}" class="img-fluid" alt="">
				</a>
			</div>
		</div>
		<!-- Modal -->
		<div class="modal fade" id="video-modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<!-- 16:9 aspect ratio -->
						<div class="embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item" src="" id="video" allowscriptaccess="always" allow="autoplay"></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>