<section class="section-seer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12  col-md-6 col-lg-4 col-xs-4">
                <div class="block-pink"></div>
                <h5 class="block-title_pink text-left">{{ $sede_titulo }}</h5>
                <div class="escritorios escritorio-adjustment_omdv">
                    @foreach($escritorios_list as $escritorio_item)
                        <div class="escritorio">
                            <div class="sede-instituto-vida">{{ $escritorio_item['title'] }}
                                <div>{!! $escritorio_item['endereco'] !!}</div>
                            </div>
                            <div class="sede-instituto-vida">{{ __('Diretor local:', 'instituto-viva') }}
                                <div>{{ $escritorio_item['diretor'] }}</div>
                            </div>
                            <div class="sede-instituto-vida">{{ __('E-mail:', 'instituto-viva') }}
                                <div>{{ $escritorio_item['e-mail'] }}</div>
                            </div>
                            @if($escritorio_item['telefone'])
                                <div class="sede-instituto-vida">{{ __('Tel.:', 'instituto-viva') }}
                                    <div>{{ $escritorio_item['telefone'] }}</div>
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8 col-xl-8">
                <div class="map-seer d-none d-xs-none d-sm-none d-md-block">
                    <img class="img-fluid" src="@asset('images/temp/map.png')" alt="">
                </div>
                <div class="info-seer">
                    <div class="sede-icon-seer">
                        <img class="img-fluid" src="@asset('images/temp/pin-brasilia-copy.svg')" alt="">
                    </div>
                </div>
                <div class="sede-info-seer">
                    <div class="sede-instituto-vida">{{ __('SEDE', 'instituto-viva') }}</div>
                    <div class="sede-instituto-vida">{{ $escritorios_sede['title'] }}
                        <div>{!! $escritorios_sede['endereco'] !!}</div>
                    </div>
                    <div class="sede-instituto-vida">{{ __('Diretor local:', 'instituto-viva') }}
                        <div>{{ $escritorios_sede['diretor'] }}</div>
                    </div>
                    <div class="sede-instituto-vida">{{ __('E-mail:', 'instituto-viva') }}
                        <div>{{ $escritorios_sede['e-mail'] }}</div>
                    </div>
                    <div class="sede-instituto-vida">{{ __('Tel.:', 'instituto-viva') }}
                        <div>{{ $escritorio_item['telefone'] }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>