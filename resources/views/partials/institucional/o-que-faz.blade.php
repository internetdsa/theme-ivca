<section class="o-que-faz">
    <div class="container">
        <div class="row">
            {{-- Estrutura do retangulo e título com imagens ao redor --}}
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                <div class="rectangle-omdv">
                    <h3 class="paragraph-moqfi text-left">{{ $o_que_faz_titulo }}</h3>
                </div>
                <div class="img-right__omdv d-none d-xs-none d-sm-none d-md-block">
                    <img src="{{ $o_que_faz_url_imagem_lateral_1 }}" class="img-fluid" alt="">
                </div>
                <div class="img-bottom__omdv d-none d-xs-none d-sm-none d-md-block">
                    <img src="{{ $o_que_faz_url_imagem_lateral_2 }}" class="img-fluid" alt="">
                </div>
            </div>
            {{-- Texto lateral do retangulo e título com imagens ao redor --}}
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                <div class="paragraph-ctad">
                    {!! $o_que_faz_resumo !!}
                </div>
            </div>
        </div>
    </div>
</section>