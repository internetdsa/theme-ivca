<section class="lista-de-posts">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				@if(!$posts_list_erro)
					@foreach ($posts_list as $post)
					<div class="item">
						<div class="container-img">
							<img src="{{ $post['thumbnail'] }}" alt="">
						</div>
						<div class="container-text">
							<h2 class="title">{!! $post['title'] !!}</h2>
							{!! $post['resumo'] !!}
							<a href="{{ $post['url_saiba_mais'] }}" target="_blank">{{ __('VEJA MAIS', 'instituto-viva') }}<img src="@asset('images/icon-plus-blue.png')" alt="">
								</a>
						</div>
					</div>
					@endforeach

					<ul class="pagination">
						@if ($posts_list_pagina_atual > 1)
							<li class="page-item"><a class="page-link" href="{{ get_permalink() . '?pagina=' . ($posts_list_pagina_atual-1) }}">{{ __('Anterior', 'instituto-viva') }}</a></li>
						@endif
						@for ($i = 1; $i < 4; $i++)
						<li class="page-item{{ $i == $posts_list_pagina_atual ? ' active' : '' }}">
							<a class="page-link" href="{{ get_permalink() . '?pagina=' . $i }}">{{ $i }}</a>
						</li>
						@endfor
						@if ($posts_list_pagina_atual < 7)
							<li class="page-item"><a class="page-link" href="{{ get_permalink() . '?pagina=' . ($posts_list_pagina_atual+1) }}">{{ __('Próxima', 'instituto-viva') }}</a></li>
						@endif
					</ul>
				@else
					<div class="item">
						<div class="container-text">
							<h2 class="title">{{ __('Nenhuma dica encontrada', 'instituto-viva') }}</h2>
							{{ __('Nossos servidores não conseguiram encontrar nenhuma dica, tente novamente mais tarde.', 'instituto-viva') }}
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</section>
