<div class="card-repeat col-xs-12 col-sm-12 col-md-6 col-lg-3 col-xl-3">
    <div class="card card-consultant__setting card-bkg">
        <img class="img-fluid" src="{{ $area_item['thumbnail'] }}" alt="">
        <div class="card-body">
            <h5 class="card-title_consultant text-left">{{ $area_item['title'] }}</h5>
            @if(array_key_exists('nivel', $area_item) and !empty($area_item['nivel']))
                <p class="card-subtitle_consultant text-left">{{ $area_item['nivel'] }}</p>
            @endif
            <hr class="card-line">
            <div class="card-description">
                @if(array_key_exists('categorias', $area_item) and !empty($area_item['categorias']))
                    {!! $area_item['categorias'] !!}
                @endif
                @if(array_key_exists('resumo', $area_item) and !empty($area_item['resumo']))
                    {!! $area_item['resumo'] !!}
                @endif
            </div>
            @if(array_key_exists('url_saiba_mais', $area_item) and !empty($area_item['url_saiba_mais']))
                <a href="{{ $area_item['url_saiba_mais'] }}" class="btn btn-pink">{{ __('SAIBA MAIS', 'instituto-viva') }} <img src="@asset('images/icon-plus-white.png')" alt=""></a>
            @endif
        </div>
    </div>
</div>