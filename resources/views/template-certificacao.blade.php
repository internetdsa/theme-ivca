{{--
  Template Name: Certificação
--}}

@extends('layouts.app')

@section('content')
  @include('partials.hero')
  @include('partials.certificacao-ivca.slider-lateral')
  @include('partials.certificacao-ivca.accordion')
  @include('partials.contato.agende-visita')
@endsection
