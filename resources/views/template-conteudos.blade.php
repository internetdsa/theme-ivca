{{--
  Template Name: Conteúdos
--}}

@extends('layouts.app')

@section('content')
  @include('partials.hero')
  @include('partials.geral.areas')
  @include('partials.conteudos.texto-com-imagem-lateral')
  @include('partials.contato.agende-visita')
@endsection
