{{--
  Template Name: Consultores
--}}

@extends('layouts.app')

@section('content')
  @include('partials.hero')
  @include('partials.consultores.card-com-botao')
  @include('partials.consultores.text-com-img-esquerda')
  @include('partials.contato.agende-visita')
@endsection
