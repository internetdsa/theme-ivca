{{--
  Template Name: Empresas certificadas
--}}

@extends('layouts.app')

@section('content')
  @include('partials.hero')
  @include('partials.empresas-certificadas.card-logos')
  @include('partials.contato.agende-visita')
@endsection
