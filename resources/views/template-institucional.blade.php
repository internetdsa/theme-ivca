{{--
  Template Name: Institucional
--}}

@extends('layouts.app')

@section('content')
  @include('partials.hero')
  @include('partials.institucional.o-melhor-da-vida')
  @include('partials.institucional.o-que-faz')
  @include('partials.geral.areas')
  @include('partials.institucional.sede')
  @include('partials.contato.agende-visita')
@endsection
