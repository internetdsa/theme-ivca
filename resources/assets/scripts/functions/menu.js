$(function() {
    const header = $('header');

    function checkMenuOpacity(header) {
        let scroll = $(window).scrollTop();
        if (scroll >= 50) {
            header.addClass('active');
        } else {
            if(!$('.navbar-collapse').hasClass('show'))
                header.removeClass('active');
        }
    }


    // Verifica se é necessário remover opacidade do menu
    checkMenuOpacity(header);
    $(window).scroll(function() {
        checkMenuOpacity(header);
    });

    // Remove opacidade do menu ao abrir menu mobile
    $('.navbar-toggler').click(function () {
        if(!header.hasClass('active')) {
            header.addClass('active');
        } else {
            let scroll = $(window).scrollTop();
            if (scroll < 50) {
                header.removeClass('active');
            }
        }
    });
});
