$(window).on('load', function(){
	resizeCarrocel();
	$('.owl-carousel').owlCarousel({
		loop: true,
		margin: 20,
		nav: false,
		dots: false,
		responsive: {
			0: {
				items: 3,
			},
			1300: {
				items: 4,
			},
			1700: {
				items: 5,
			},
			2050: {
				items: 6,
			},
		},
	});
});

function resizeCarrocel() {
	let w_container = $('.container').width();
	let w_col = $('.col-lg-5').width();
	let w_item = $('.owl-item').width();
	let w_windows = $(this).width();

	$('.itens-carousel').width((w_col+((w_windows-w_container)/2)+(w_item/2)));
}

$(window).on('resize', function(){
	resizeCarrocel();
});